import os
import hashlib
import argparse

def parse_if_file_valid(file_path):
    if not os.path.exists(file_path):
        raise argparse.ArgumentTypeError("File %s does not exist!" % file_path)
    else:
        return file_path

def generate_hash(key):
    return int(hashlib.md5(key.encode('utf-8')).hexdigest(), 16)

def read_data(file_path):
    data = {}
    with open(file_path, 'r') as file:
        for line in file:
            items = line.strip().split(' ')
            key, value = items[0], " ".join(items[1:])
            data[key] = value
    return data
