import argparse
import requests
from flask import Flask, jsonify
from util import read_data, parse_if_file_valid

data = {}

app = Flask(__name__)

# TODO: Instead of hard-coding use app config
SERVER_CONFIG = {
    '0': 'http://localhost:7070',
    '1': 'http://localhost:7070',
    '2': 'http://localhost:7070',
    '3': 'http://localhost:7070',
    '4': 'http://localhost:7070'
}

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--data-file", required=True, type=parse_if_file_valid, help="Data file path")
    parser.add_argument("-p", "--port", default=6060, type=int, help="Port on which to run the server")
    return parser.parse_args()

@app.route("/key/<key>")
def get_value(key):
    try:
        # TODOs:
        # 1. Optimize by checking UUID format for key thereby avoiding lookup & return 400
        # 2. Maintain LRU based lookup table to reduce the size of table in memory
        # 3. Use only 32/64 bits of UUID to maintain the lookup
        server = data[key]
        server_url = SERVER_CONFIG[server]
        response = requests.get(f'{server_url}/key/{key}')
        return response.json()
    except KeyError as e:
        return jsonify("Value not found"), 404

if __name__ == '__main__':
    parsed_args = parse_arguments()
    data = read_data(parsed_args.data_file)
    app.run(port=parsed_args.port)
