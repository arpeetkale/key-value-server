# key-value-server

1. `data_processor.py` - takes data file with key-value pairs and creates shards & lookup table
2. `main_server.py`    - source of truth that fronts the entire data file (similar to a service fronting database)
3. `cache_server.py`   - takes shard file and exposes a `GET /key/<key>` API
4. `routing_server.py` - takes lookup table file and exposes a `GET /key/<key>` API which
                         looks for the key in lookup table to find the cache server that can serve the value for that key
4. `util.py`           - util functions for reading data & generating hashes


#### Steps:

1. Create shard & lookup table
`python3 data_processor.py -f example.data`
2. Start main server with example.data file
`python3 main_server.py -f example.data`
3. Start cache server with shard-0 file
`python3 cache_server.py -f shard-0`
4. Start routing server with lookup table
`python3 routing_server.py -f key-server-lookup`
5. Test key lookup

- Cache-Hit
```
export test_shard_0_key=$(head -n 1 shard-0 | cut -d' ' -f 1)
curl http://localhost:6060/key/$test_shard_0_key
```

- Cache Miss
```
export test_shard_1_key=$(head -n 1 shard-1 | cut -d' ' -f 1)
curl http://localhost:6060/key/$test_shard_1_key
```
