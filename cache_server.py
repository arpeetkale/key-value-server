import argparse
import requests
from flask import Flask, jsonify
from util import read_data, parse_if_file_valid

# TODO: Instead of hard-coding use app config
_MAIN_SERVER_URL = "http://localhost:8080"
data = {}

app = Flask(__name__)

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--data-file", required=True, type=parse_if_file_valid, help="Data file path")
    parser.add_argument("-p", "--port", default=7070, type=int, help="Port on which to run the server")
    return parser.parse_args()

@app.route("/key/<key>")
def get_value(key):
    try:
        # TODO: Optimize by check UUID format for key thereby avoiding lookup & return 400
        return { "value": data[key] }
    except KeyError as e:
        response = requests.get(f'{_MAIN_SERVER_URL}/key/{key}')
        if response.status_code == 200:
            return response.json()
        elif response.status_code == 404:
            return jsonify("Value not found"), 404
        else:
            return jsonify("Server Error"), 500

if __name__ == '__main__':
    parsed_args = parse_arguments()
    data = read_data(parsed_args.data_file)
    app.run(port=parsed_args.port)
