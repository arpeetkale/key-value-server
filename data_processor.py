import pprint
import argparse
from util import *

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--data-file", required=True, type=parse_if_file_valid, help="Data file path")
    parser.add_argument("-n", "--number-of-nodes", required=False, type=int, help="Number of nodes", default=5)
    return parser.parse_args()

def create_lookup_table_and_shards(data, number_of_nodes):
    key_to_node_map = {}
    node_to_key_value_pair_map = {}

    for i in range(number_of_nodes):
        node_to_key_value_pair_map[i] = []

    # Create a dictionary of key & node pairs
    # This way given a request for a key it can be routed to
    # the node that can serve that key's value
    for key in data.keys():
        key_hash = generate_hash(key)
        node = key_hash % number_of_nodes
        key_to_node_map[key] = node

    # Create a map of node to key,value pair lists
    # This is for each node to maintain it's cache of key,value pairs
    for key in data.keys():
        node = key_to_node_map[key]
        node_key_list = node_to_key_value_pair_map[node]
        node_key_list.append((key, data[key]))
        node_to_key_value_pair_map[node] = node_key_list

    # Write lookup table for key to node pairs
    with open('key-server-lookup', 'w') as lookup_file:
        for k, v in key_to_node_map.items():
            lookup_file.write(f'{k} {v}\n')

    # Write individual shard files with key value pairs
    for node in node_to_key_value_pair_map.keys():
        key_value_pair_list = node_to_key_value_pair_map[node]
        with open(f'shard-{node}', 'w') as shard_file:
            for k, v in key_value_pair_list:
                shard_file.write(f'{k} {v}\n')


if __name__ == '__main__':
    parsed_args = parse_arguments()
    data = read_data(parsed_args.data_file)
    create_lookup_table_and_shards(data, parsed_args.number_of_nodes)
